/**
 * 
 */
package gov.llamaland.birthdays;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Greg
 *
 */
public class BirthdayEmail {
	private final static int BIRTHDAY_NUMBER = 100; // hundredth birthday
	private final static int LOOK_AHEAD_DAYS = 14; // enough for ten weekdays
													// (ignoring holidays)
	private final static int SHOW_DAYS = 7; // enough for five weekdays
											// (ignoring holidays)
	private final static int BUSY_DAY = 20; // more than this number of birthdays requires advanced warning

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 2) {
			System.out
					.println("This program creates a list of people who are nearing their one hundredth birthday.\nIt takes two filenames as parameters: \na file with the list of citizens and a file with a list of email addresses to be excluded'");
		} else {
			String citizens = args[0];
			String exclusions = args[1];
			listBirthdays(citizens, exclusions);
		}
	}

	protected static LocalDate[] birthdatesOfInterest(LocalDate today,
			int daysAhead, int birthdayNumber) {
		LocalDate[] dates = new LocalDate[2];
		dates[0] = today.minus(birthdayNumber, ChronoUnit.YEARS);
		dates[1] = dates[0].plus(daysAhead, ChronoUnit.DAYS);
		return dates;

	}

	protected static void listBirthdays(String citizensFile,
			String exclusionsFile) {
		LocalDate today = LocalDate.now();
		listBirthdays(citizensFile, exclusionsFile, today);
	}

	protected static void listBirthdays(String citizensFile,
			String exclusionsFile, LocalDate today) {
		LocalDate[] birthdates = birthdatesOfInterest(today, LOOK_AHEAD_DAYS,
				BIRTHDAY_NUMBER);
		Set<String> emailAddresses = null;
		Set<Citizen> citizens = null;
		try (Stream<String> stream = Files.lines(Paths.get(citizensFile))) {

			// find all citizens who might require an email
			citizens = bornBetween(stream, birthdates[0], birthdates[1]);
			emailAddresses = citizens.stream().map(c -> c.getEmail())
					.collect(Collectors.toSet());
		} catch (IOException e) {
			System.out.println("Cannot read file: " + citizensFile);
			System.exit(1);
		}

		// filter out email addresses from the exclusions file

		try (Stream<String> exclusionStream = Files.lines(Paths
				.get(exclusionsFile))) {
			exclude(exclusionStream, emailAddresses);
		} catch (IOException e) {
			System.out
					.println("Cannot read exclusions file: " + exclusionsFile);
			System.exit(1);
		}

		// filter out email addresses that are duplicates

		try (Stream<String> stream = Files.lines(Paths.get(citizensFile))) {
			removeDuplicates(stream, emailAddresses);
		} catch (IOException e) {
			System.out.println("Cannot read file: " + citizensFile);
			System.exit(1);
		}
		// remove any people whose email is not on the list

		try (Stream<Citizen> stream = citizens.stream()) {
			printReport(stream, emailAddresses, today);
		}

	}

	protected static void printReport(Stream<Citizen> stream,
			Set<String> emailAddresses, LocalDate today) {
		List<Citizen> birthdayPeople = stream.filter(
				c -> emailAddresses.contains(c.getEmail())).collect(
				Collectors.toList());
		LocalDate lastDate = null;
		List<Citizen> dayList = null;
		// sort and display results
		Collections.sort(birthdayPeople);
		for (Citizen person : birthdayPeople) {
			if (!person.getBirthdate().equals(lastDate)) {
				if (dayList != null && dayList.size() > 0) {
					printDay(dayList, today, lastDate);				
				}
				dayList = new LinkedList<Citizen>();
				lastDate = person.getBirthdate();
				dayList.add(person);
			}
		}
		if (dayList != null && dayList.size() > 0) {
			printDay(dayList, today, lastDate);				
		}
		
	}

	protected static void printDay(List<Citizen> list, LocalDate today, LocalDate birthdate) {
		LocalDate birthday = birthdate.plusYears(100);
		int days = Period.between(today, birthday ).getDays();
		if (days <= BirthdayEmail.SHOW_DAYS) {
			System.out.println(birthday.format(Citizen.formatter));
			for (Citizen person : list) {
				System.out.println(person);
			}
		} else {
			int size = list.size();
			if (size > BirthdayEmail.BUSY_DAY) {
				System.out.println("Upcoming busy day with " + size + " birthdays: " + birthday.format(Citizen.formatter));
				for (Citizen person : list) {
					System.out.println(person);
				}
			}
		}
		
		System.out.println();
	}

	protected static void removeDuplicates(Stream<String> stream,
			Set<String> emailAddresses) {
		Map<String, Long> map = stream
				.map(s -> s.substring(s.lastIndexOf(",") + 1).trim())
				.filter(e -> emailAddresses.contains(e))
				.collect(
						Collectors.groupingBy(Function.identity(),
								Collectors.counting()));
		;
		for (Map.Entry<String, Long> entry : map.entrySet()) {
			if (entry.getValue() > 1) {
				emailAddresses.remove(entry.getKey());
			}

		}
	}

	protected static void exclude(Stream<String> exclusionsStream,
			Set<String> emailAddresses) {
		exclusionsStream.forEach(s -> emailAddresses.remove(s.substring(
				s.lastIndexOf(",") + 1).trim()));
	}

	protected static Set<Citizen> bornBetween(Stream<String> lines,
			LocalDate start, LocalDate end) {
		LocalDate endPlus1 = end.plusDays(1);
		LocalDate startMinus1 = start.minusDays(1);
		Set<Citizen> citizens = lines
				.map(s -> new Citizen(s))
				.filter(c -> c.getBirthdate().isAfter(startMinus1)
						&& c.getBirthdate().isBefore(endPlus1))
				.collect(Collectors.toSet());
		return citizens;
	}

}
