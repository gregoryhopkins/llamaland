package gov.llamaland.birthdays;
import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.Period;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 */

/**
 * @author Greg
 *
 */
public class BirthdayEmailTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testbirthdatesOfInterest() {
		LocalDate today = LocalDate.of(2065, 4, 1);
		LocalDate[] startAndEnd = BirthdayEmail.birthdatesOfInterest(today, 3, 100);
	    assertEquals(startAndEnd[1],LocalDate.of(1965, 4, 1));
	    assertEquals(startAndEnd[0],LocalDate.of(1965, 3, 29));	
	}
	
	@Test
	public void testBornBetween1() {
		String[] citizens = {"Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com",
				             "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com",
				             "Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land"};
		try (Stream<String> stream = Arrays.stream(citizens)) {
			Set<Citizen> chosen = BirthdayEmail.bornBetween(stream, LocalDate.of(1900, 1, 1), LocalDate.of(1901, 1, 1));	
			
			assertEquals(chosen.size(),1);
			assertTrue(chosen.contains(new Citizen("O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com")));
		}
	}
	
	@Test
	public void testBornBetween2() {
		String[] citizens = {"Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com",
				             "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com",
				             "Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land"};
		try (Stream<String> stream = Arrays.stream(citizens)) {
			Set<Citizen> chosen = BirthdayEmail.bornBetween(stream, LocalDate.of(1900, 2, 28), LocalDate.of(1901, 2, 28));	
			assertEquals(chosen.size(),1);
			assertTrue(chosen.contains(new Citizen("O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com")));
	
		}
	}
	
	@Test
	public void testBornBetween3() {
		String[] citizens = {"Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com",
				             "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com",
				             "Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land"};
		try (Stream<String> stream = Arrays.stream(citizens)) {
			Set<Citizen> chosen = BirthdayEmail.bornBetween(stream, LocalDate.of(1900, 03, 01), LocalDate.of(1901, 2, 28));	
			assertEquals(chosen.size(),0);
		}
	}
	
	@Test
	public void testBornBetween4() {
		String[] citizens = {"Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com",
				             "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com",
				             "Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land"};
		try (Stream<String> stream = Arrays.stream(citizens)) {
			Set<Citizen> chosen = BirthdayEmail.bornBetween(stream, LocalDate.of(1900, 01, 01), LocalDate.of(1960, 2, 28));	
			assertEquals(chosen.size(),3);
		}
	}
	
	@Test
	public void testExclude() {
		String[] citizens = {"Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com",
	             "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com",
	             "Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land"};
		String[] toExclude = { "bobby.brown@ilovellamaland.com",
	             "betsy@heyitsme.com" };
		try (Stream<String> stream = Arrays.stream(citizens)) {
			Set<String> emailAddresses = 
				stream.map( s -> s.substring(s.lastIndexOf(",") + 1).trim())
				      .collect(Collectors.toSet());
			assertEquals(emailAddresses.size(),3);
			assertTrue(emailAddresses.contains("bobby.brown@ilovellamaland.com"));
			BirthdayEmail.exclude(Arrays.stream(toExclude), emailAddresses);
			assertEquals(emailAddresses.size(),1);
			assertFalse(emailAddresses.contains("bobby.brown@ilovellamaland.com"));
		}
	}
}
