package gov.llamaland.birthdays;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.Period;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 */

/**
 * @author Greg
 *
 */
public class BirthdayEmailTest {

    
    // Test date of birthday correctly calculated from today, lookahead and birthday
    @Test
    public void testbirthdatesOfInterest() {
        LocalDate today = LocalDate.of(2065, 4, 1);
        LocalDate[] startAndEnd = BirthdayEmail.birthdatesOfInterest(today, 3,
                100);
        assertEquals(LocalDate.of(1965, 4, 1), startAndEnd[0]);
        assertEquals(LocalDate.of(1965, 4, 4), startAndEnd[1]);
    }

    
    // test born betwen for an exact date
    @Test
    public void testBornBetween1() {
        String[] citizens = {
                "Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com",
                "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com",
                "Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land" };
        try (Stream<String> stream = Arrays.stream(citizens)) {
            Set<Citizen> chosen = BirthdayEmail.bornBetween(stream,
                    LocalDate.of(1900, 1, 1), LocalDate.of(1901, 1, 1));

            assertEquals(chosen.size(), 1);
            assertTrue(chosen.contains(new Citizen(
                    "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com")));
        }
    }

    // Test that born between can identify single citizen
    @Test
    public void testBornBetween2() {
        String[] citizens = {
                "Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com",
                "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com",
                "Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land" };
        try (Stream<String> stream = Arrays.stream(citizens)) {
            Set<Citizen> chosen = BirthdayEmail.bornBetween(stream,
                    LocalDate.of(1900, 2, 28), LocalDate.of(1901, 2, 28));
            assertEquals(chosen.size(), 1);
            assertTrue(chosen.contains(new Citizen(
                    "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com")));

        }
    }

    // Test that born between works if no citizens were born between the dates
    @Test
    public void testBornBetween3() {
        String[] citizens = {
                "Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com",
                "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com",
                "Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land" };
        try (Stream<String> stream = Arrays.stream(citizens)) {
            Set<Citizen> chosen = BirthdayEmail.bornBetween(stream,
                    LocalDate.of(1900, 03, 01), LocalDate.of(1901, 2, 28));
            assertEquals(chosen.size(), 0);
        }
    }

    
    // Test that all are included if they are between the two dates
    @Test
    public void testBornBetween4() {
        String[] citizens = {
                "Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com",
                "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com",
                "Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land" };
        try (Stream<String> stream = Arrays.stream(citizens)) {
            Set<Citizen> chosen = BirthdayEmail.bornBetween(stream,
                    LocalDate.of(1900, 01, 01), LocalDate.of(1960, 2, 28));
            assertEquals(chosen.size(), 3);
        }
    }

    // test that emails in the exclusion list are removed
    @Test
    public void testExclude() {
        String[] citizens = {
                "Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com",
                "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com",
                "Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land" };
        String[] toExclude = { "bobby.brown@ilovellamaland.com",
                "betsy@heyitsme.com" };
        try (Stream<String> stream = Arrays.stream(citizens)) {
            Set<String> emailAddresses = stream.map(
                    s -> s.substring(s.lastIndexOf(",") + 1).trim()).collect(
                    Collectors.toSet());
            assertEquals(emailAddresses.size(), 3);
            assertTrue(emailAddresses
                    .contains("bobby.brown@ilovellamaland.com"));
            BirthdayEmail.exclude(Arrays.stream(toExclude), emailAddresses);
            assertEquals(1, emailAddresses.size());
            assertFalse(emailAddresses
                    .contains("bobby.brown@ilovellamaland.com"));
        }
    }

    
    // Test that the system doesn't include people who have duplicate email addresses
    @Test
    public void testRemoveDuplicates() {
        String[] citizens = {
                "Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com",
                "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com",
                "Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land",
                "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com",
                "Corleone,Don,01-01-1927,alfie@vontappo.llama.land" };

        String[] emails = { "bobby.brown@ilovellamaland.com",
                "betsy@heyitsme.com" };

        Set<String> emailAddresses = new HashSet<String>(Arrays.asList(emails));

        try (Stream<String> stream = Arrays.stream(citizens)) {
            assertEquals(2, emailAddresses.size());
            BirthdayEmail.removeDuplicates(stream, emailAddresses);
            assertEquals(1, emailAddresses.size());
            assertFalse(emailAddresses.contains("betsy@heyitsme.com"));
            assertFalse(emailAddresses.contains("alfie@vontappo.llama.land"));
            assertTrue(emailAddresses
                    .contains("bobby.brown@ilovellamaland.com"));
        }
    }

   
    // A complete test of the system reading from streams and writing
    // to standard output
    @Test
    public void testListBirthdays() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream testOut = new PrintStream(outContent);
        PrintStream originalOut = System.out;
        System.setOut(testOut);
        BirthdayEmail.listBirthdays("src/test/resources/citizens1.txt",
                "src/test/resources/exclusions1.txt",
                LocalDate.of(2019, 12, 30));
        assertTrue(outContent.toString().contains("alfie@vontappo.llama.land"));
        System.setOut(originalOut);
    }
   
    
    // Test that leap year birthdays get an email on their 25th birthday
    // assuming that King Tom follows the example of Queen Elizabeth II
    @Test
    public void testLeapYears() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream testOut = new PrintStream(outContent);
        PrintStream originalOut = System.out;
        System.setOut(testOut);
        
        BirthdayEmail.listBirthdays("src/test/resources/citizensLeap.txt",
                "src/test/resources/exclusions1.txt",
                LocalDate.of(2024, 02, 26));
        assertTrue(outContent.toString().contains("betsy@notexcluded.com"));
        System.setOut(originalOut);
    }
    
    
    //
    @Test
    public void testAdvanceNotice() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream testOut = new PrintStream(outContent);
        PrintStream originalOut = System.out;
        //System.setOut(testOut);
        
        BirthdayEmail.listBirthdays("src/test/resources/citizensAdvance.txt",
                "src/test/resources/exclusions1.txt",
                LocalDate.of(2021, 01, 01));
       // assertTrue(outContent.toString().contains("large"));
        System.setOut(originalOut);
    }
    
 
    // test that exception is thrown with bad data
    @Test
    public void testBadData() {
        boolean thrown = false;
        String[] citizens = {
                "Brown,,Bobby,10-11-1950,bobby.brown@ilovellamaland.com",
                "O'Rourke,Betsy,28-02-1900",
                "Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land" };
        try (Stream<String> stream = Arrays.stream(citizens)) {
            Set<Citizen> chosen = BirthdayEmail.bornBetween(stream,
                    LocalDate.of(1900, 1, 1), LocalDate.of(1901, 1, 1));
    
           
        } catch (Exception e) {
            thrown = true;
        }
    }
}
