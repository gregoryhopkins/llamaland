
package gov.llamaland.birthdays;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Creates a list of people who have upcoming hundredth birthdays. Takes two parameters:
 * a file with the list of citizens and a file with a list of email addresses to be excluded.
 * It lists all people whose hundredth birthday is within 5 weekdays. If there are more than
 * twenty people having a hundredth birthday on the same day within 10 weekdays then it gives
 * advance warming that there are a lot of birthdays coming up. If any email addresses appear
 * more than once in the citizens file, then people with that email are ignored
 * 
 * This program reads through the entire citizens file each day and stops with an exception
 * if it cannot read the records in the file. For efficiency I would recommend processing the
 * citizen's file first so that it only contains valid records for people older than 98 who 
 * don't have a duplicate email address in the file. This would make the program run about
 * one thousand times faster, but is not part of the specification so I have not implemented
 * this here (it is simple to do using the functions below).
 * 
 * @author Greg
 *
 */
public class BirthdayEmail {
    private final static int BIRTHDAY_NUMBER = 100; // hundredth birthday
    protected static int LOOK_AHEAD_DAYS = 14; // enough for ten weekdays
                                                   // (ignoring holidays)
    private final static int SHOW_DAYS = 7; // enough for five weekdays
                                            // (ignoring holidays)
    private final static int BUSY_DAY = 20; // more than this number of
                                            // birthdays requires advanced
                                            // warning

    /**
     * main function - Takes two parameters:a file with the list of citizens and a file with a list of email addresses to be excluded.
     * Citizens file format for each line is: last name,first name,birthdate,email address
     * for example: O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com
     * Excluded email file is one email address per line: betsy@heyitsme.com
     * 
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out
                    .println("This program creates a list of people who are nearing their one hundredth birthday.\nIt takes two filenames as parameters: \na file with the list of citizens and a file with a list of email addresses to be excluded.");
        } else {
            String citizens = args[0];
            String exclusions = args[1];
            listBirthdays(citizens, exclusions);
        }
    }

    
    /**
     * Returns an array of two date: 100 years before today and daysAhead days from this date.
     * 
     * @param today
     * @param daysAhead
     * @param birthdayNumber
     * @return
     */
    protected static LocalDate[] birthdatesOfInterest(LocalDate today,
            int daysAhead, int birthdayNumber) {
        LocalDate[] dates = new LocalDate[2];
        dates[0] = today.minus(birthdayNumber, ChronoUnit.YEARS);
        dates[1] = dates[0].plus(daysAhead, ChronoUnit.DAYS);
        return dates;

    }

    /**
     * Produces a report containing upcoming birthdays based on the current date and the two files.
     * 
     * @param citizensFile
     * @param exclusionsFile
     */
    protected static void listBirthdays(String citizensFile,
            String exclusionsFile) {
        LocalDate today = LocalDate.now();
        listBirthdays(citizensFile, exclusionsFile, today);
    }
    
    /**
     * Produces a report containing upcoming birthdays based on the specified date and the two files.
     * 
     * @param citizensFile
     * @param exclusionsFile
     * @param today
     */
    protected static void listBirthdays(String citizensFile,
            String exclusionsFile, LocalDate today) {
        LocalDate[] birthdates = birthdatesOfInterest(today, LOOK_AHEAD_DAYS,
                BIRTHDAY_NUMBER);
        Set<String> emailAddresses = null;
        Set<Citizen> citizens = null;
        try (Stream<String> stream = Files.lines(Paths.get(citizensFile))) {

            // find all citizens who might require an email
            citizens = bornBetween(stream, birthdates[0], birthdates[1]);
            emailAddresses = citizens.stream().map(c -> c.getEmail())
                    .collect(Collectors.toSet());
        } catch (IOException e) {
            System.out.println("Cannot read file: " + citizensFile);
            System.exit(1);
        }

        // filter out email addresses from the exclusions file

        try (Stream<String> exclusionStream = Files.lines(Paths
                .get(exclusionsFile))) {
            exclude(exclusionStream, emailAddresses);
        } catch (IOException e) {
            System.out
                    .println("Cannot read exclusions file: " + exclusionsFile);
            System.exit(1);
        }

        // filter out email addresses that are duplicates

        try (Stream<String> stream = Files.lines(Paths.get(citizensFile))) {
            removeDuplicates(stream, emailAddresses);
        } catch (IOException e) {
            System.out.println("Cannot read file: " + citizensFile);
            System.exit(1);
        }
        // remove any people whose email is not on the list

        try (Stream<Citizen> stream = citizens.stream()) {
            printReport(stream, emailAddresses, today);
        }

    }

    /**
     * Prints a report by including all citizens in the stream with email
     * addresses in the specified set and calling printDay for each day. 
     * 
     * @param stream
     * @param emailAddresses
     * @param today
     */
    protected static void printReport(Stream<Citizen> stream,
            Set<String> emailAddresses, LocalDate today) {
        System.out.println("Upcoming birthdays:\n");
        List<Citizen> birthdayPeople = stream.filter(
                c -> emailAddresses.contains(c.getEmail())).collect(
                Collectors.toList());
        LocalDate lastDate = null;
        List<Citizen> dayList = null;
        // sort and display results
        Collections.sort(birthdayPeople);
        for (Citizen person : birthdayPeople) {
            if (!person.getBirthdate().equals(lastDate)) {
                if (dayList != null && dayList.size() > 0) {
                    printDay(dayList, today, lastDate);
                }
                dayList = new LinkedList<Citizen>();
                lastDate = person.getBirthdate();
                
            }
            dayList.add(person);
        }
        if (dayList != null && dayList.size() > 0) {
            printDay(dayList, today, lastDate);
        }
    }

    /**
     * Prints out the report for each day. If the birthday is within SHOW_DAYS
     * time it lists each citizen. If the birthday is later than SHOW_DAYS away
     * it only lists the citizens if it is a busy day (more than BUSY_DAY birthdays)
     * 
     * @param list
     * @param today
     * @param birthdate
     */
    protected static void printDay(List<Citizen> list, LocalDate today,
            LocalDate birthdate) {
        LocalDate birthday = birthdate.plusYears(100);
        int days = Period.between(today, birthday).getDays();
        if (days <= BirthdayEmail.SHOW_DAYS) {
            System.out.println(birthday.format(Citizen.formatter));
            for (Citizen person : list) {
                System.out.println(person);
            }
        } else {
            int size = list.size();
            if (size > BirthdayEmail.BUSY_DAY) {
                System.out.println("Upcoming busy day with " + size
                        + " birthdays: " + birthday.format(Citizen.formatter));
                for (Citizen person : list) {
                    System.out.println(person);
                }
            }
        }
        System.out.println();
    }

    /**
     * Removes email addresses from the specified set if they appear more than once in 
     * the specified stream.
     * 
     * @param stream
     * @param emailAddresses
     */
    protected static void removeDuplicates(Stream<String> stream,
            Set<String> emailAddresses) {
        Map<String, Long> map = stream
                .map(s -> s.substring(s.lastIndexOf(",") + 1).trim())
                .filter(e -> emailAddresses.contains(e))
                .collect(
                        Collectors.groupingBy(Function.identity(),
                                Collectors.counting()));
        ;
        for (Map.Entry<String, Long> entry : map.entrySet()) {
            if (entry.getValue() > 1) {
                emailAddresses.remove(entry.getKey());
            }

        }
    }
    
    /**
     * Removes email addresses from the specified set if they appear in the
     * stream of email addresses to be excluded.
     * 
     * @param exclusionsStream
     * @param emailAddresses
     */
    protected static void exclude(Stream<String> exclusionsStream,
            Set<String> emailAddresses) {
        exclusionsStream.forEach(s -> emailAddresses.remove(s.substring(
                s.lastIndexOf(",") + 1).trim()));
    }
    
    /**
     * Creates a set of all citizens in the specified Stream whose birthdates
     * are between the specified start and end days (inclusive).
     * 
     * @param lines
     * @param start
     * @param end
     * @return
     */
    protected static Set<Citizen> bornBetween(Stream<String> lines,
            LocalDate start, LocalDate end) {
        LocalDate endPlus1 = end.plusDays(1);
        LocalDate startMinus1 = start.minusDays(1);
        Set<Citizen> citizens = lines
                .map(s -> new Citizen(s))
                .filter(c -> c.getBirthdate().isAfter(startMinus1)
                        && c.getBirthdate().isBefore(endPlus1))
                .collect(Collectors.toSet());
        return citizens;
    }

}
