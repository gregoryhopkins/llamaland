package gov.llamaland.birthdays;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

public class Citizen implements Comparable<Citizen>{
	
	private String last;
	private String first;
	private LocalDate birthdate;
	private String email;
    protected static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	
	/**
	 * Creates a citizen from a String in the format last name, first name, birthdate, email
	 * for example: "Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com"
	 * Reports any line that causes an exception to aid data clean-up. 
	 * @param details
	 */
	
	public Citizen(String details) {
	    try {
    		String[] part = details.split(",");
    		last = part[0];
    		first = part[1];
    		birthdate = LocalDate.parse(part[2], formatter);
    		email = part[3].trim();
	    } catch (Exception e) {
	        throw new RuntimeException("Error processing: " + details, e);
	    }
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((birthdate == null) ? 0 : birthdate.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((last == null) ? 0 : last.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Citizen other = (Citizen) obj;
		if (birthdate == null) {
			if (other.birthdate != null)
				return false;
		} else if (!birthdate.equals(other.birthdate))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (last == null) {
			if (other.last != null)
				return false;
		} else if (!last.equals(other.last))
			return false;
		return true;
	}



	public String getLast() {
		return last;
	}



	public void setLast(String last) {
		this.last = last;
	}



	public String getFirst() {
		return first;
	}



	public void setFirst(String first) {
		this.first = first;
	}



	public LocalDate getBirthdate() {
		return birthdate;
	}



	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}

    /**
     * Sort order is base on birthdate then last name, first name, then email.
     */

	@Override
	public int compareTo(Citizen citizen) {
		return Comparator.comparing(Citizen::getBirthdate)
				         .thenComparing(Citizen::getLast)
				         .thenComparing(Citizen::getFirst)
				         .thenComparing(Citizen::getEmail)
			             .compare(this, citizen);
	}



	@Override
	public String toString() {
		return "[last=" + last + ", first=" + first + ", birthdate="
				+ birthdate.format(formatter) + ", email=" + email + "]";
	}
	
}
