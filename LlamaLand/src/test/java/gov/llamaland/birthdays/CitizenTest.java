package gov.llamaland.birthdays;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.AfterClass;
import org.junit.Test;

public class CitizenTest {
    
    // test that Citizen can parse a String into the relevant data items
    @Test
    public void testStringConstructor() {
        Citizen citizen = new Citizen(
                "O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com");
        assertEquals(citizen.getLast(), "O'Rourke");
        assertEquals(citizen.getFirst(), "Betsy");
        assertEquals(citizen.getBirthdate(), LocalDate.of(1900, 2, 28));
        assertEquals(citizen.getEmail(), "betsy@heyitsme.com");
    }

}
